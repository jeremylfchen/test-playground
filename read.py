#!/usr/bin/env python3
import sys
import json

def filterOutput ():
    input_data = sys.argv[1:] =
    if input_data == sys.argv[0]:
        print(False)
    else:
        for i in input_data:
            with open(i) as json_file:
                try:
                    data = json.load(json_file)
                except:
                    print('wat')
                levels = {}
                detected = 0
                if data and 'vulnerabilities' in data:
                    detected = len(data['vulnerabilities'])
                    for item in data['vulnerabilities']:
                        if item['severity'] in levels.keys():
                            levels[item['severity']] += 1
                        else:
                            levels[item['severity']] = 1
                print(str(json_file.name) + ':\n' + 'vulnerabilities: ' + str(detected))
                for key in levels.keys():
                    print(key + ': ' + str(levels[key]))


if __name__== '__main__':
    filterOutput()

