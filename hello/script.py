#!/usr/bin/env python3
import sys
import json

def printNames ():
    input_data = sys.argv[1:]
    if input_data == sys.argv[0]:
        print(False)
    else:
        for i in input_data:
            print(i)
            with open(i) as json_file:
                data = json.load(json_file)
                print(data['name'])


if __name__== '__main__':
    printNames()


