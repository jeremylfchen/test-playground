#!/usr/bin/env php
<?php
/**
 * looks through json files and outputs
 * @return none
 */

$MIN_VULNERABILITY_COUNT = 3;

$input_data = array_slice($argv, 1);
// $result = 0;
foreach ($input_data as &$file) {
    $str = file_get_contents($file);
    $json = json_decode($str); // decode the JSON into an object
    $result = 0;
    if (!empty($json) && property_exists($json, 'vulnerabilities')) {
        $result += count($json->vulnerabilities);
    }
    echo "$file has $result errors! \n";
}
// echo $result > $MIN_VULNERABILITY_COUNT ? $result : 0;

?>